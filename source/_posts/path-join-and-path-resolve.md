---
title: path.join() and path.resolve()
tags:
---
**path** is a built-in module that provides a way for working with directory and file paths.we can use it directly without installing any additional packages.

``` JavaScript
const path = require('path');
```
**path** module provides helpful functions for manipulating paths.

**path.join()** and **path.resolve()** are simplify your file and directory operations by resolving and joining paths.

## path.join()

- In Node.js **path.join()** function is used to concatenate path segments into single path string.
- The resulting path is a comination of all the provided segments.

``` JavaScript
const path = require('path');

const joinPaths = path.join('/home/user','directory','file.txt');

// Output: /home/user/directory/file.txt
```

## path.resolve()

- **path.resolve()** resolves the path segment from right to left, starting from the current working directory.
- It accepts multiple path segments and gives absolute path.

``` JavaScript
const path = require('path');

const resolvePaths = path.join('directory','file.txt');

// output: /home/user/directory/file.txt
```

## Conclusion

- Using **path.join()** you can easily conctenate multiple path segments.
- Using **path.resolve()** enables you to resolve absolute path from relative paths or segments.
- Combining **path.join()** and **path.resolve()** allows for even more advanced path manipulations, providing flexibility and control over path operations in your Node.js project. 